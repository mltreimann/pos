package ee.ut.math.tvt.salessystem.ui.tabs;

import java.io.IOException;

import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class StockTab extends AnchorPane {

  @FXML private Button addItem;
  @FXML private TableView warehouseTableView;

  private SalesSystemModel model;

  public StockTab(SalesSystemModel model) {
	super();
	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("StockTab.fxml"));
	 fxmlLoader.setRoot(this);
	 fxmlLoader.setController(this);

	try {
		fxmlLoader.load();
	} catch (IOException exception) {
		throw new RuntimeException(exception);
	}
	
    this.model = model;
    
    warehouseTableView.setItems(model.getWarehouseTableModel());
  }
//
//  // warehouse stock tab - consists of a menu and a table
//  public Component draw() {
//    JPanel panel = new JPanel();
//    panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//
//    GridBagLayout gb = new GridBagLayout();
//    GridBagConstraints gc = new GridBagConstraints();
//    panel.setLayout(gb);
//
//    gc.fill = GridBagConstraints.HORIZONTAL;
//    gc.anchor = GridBagConstraints.NORTH;
//    gc.gridwidth = GridBagConstraints.REMAINDER;
//    gc.weightx = 1.0d;
//    gc.weighty = 0d;
//
//    panel.add(drawStockMenuPane(), gc);
//
//    gc.weighty = 1.0;
//    gc.fill = GridBagConstraints.BOTH;
//    panel.add(drawStockMainPane(), gc);
//    return panel;
//  }
//
//  // warehouse menu
//  private Component drawStockMenuPane() {
//    JPanel panel = new JPanel();
//
//    GridBagConstraints gc = new GridBagConstraints();
//    GridBagLayout gb = new GridBagLayout();
//
//    panel.setLayout(gb);
//
//    gc.anchor = GridBagConstraints.NORTHWEST;
//    gc.weightx = 0;
//
//    addItem = new JButton("Add");
//    gc.gridwidth = GridBagConstraints.RELATIVE;
//    gc.weightx = 1.0;
//    panel.add(addItem, gc);
//
//    panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//    return panel;
//  }
//
//
//  // table of the wareshouse stock
//  private Component drawStockMainPane() {
//    JPanel panel = new JPanel();
//
//    JTable table = new JTable(model.getWarehouseTableModel());
//
//    JTableHeader header = table.getTableHeader();
//    header.setReorderingAllowed(false);
//
//    JScrollPane scrollPane = new JScrollPane(table);
//
//    GridBagConstraints gc = new GridBagConstraints();
//    GridBagLayout gb = new GridBagLayout();
//    gc.fill = GridBagConstraints.BOTH;
//    gc.weightx = 1.0;
//    gc.weighty = 1.0;
//
//    panel.setLayout(gb);
//    panel.add(scrollPane, gc);
//
//    panel.setBorder(BorderFactory.createTitledBorder("Warehouse status"));
//    return panel;
//  }

}
