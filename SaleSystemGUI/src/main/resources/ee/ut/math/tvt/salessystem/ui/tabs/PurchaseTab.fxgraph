package ee.ut.math.tvt.salessystem.ui.tabs

import java.lang.*
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.SplitPane
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.control.TitledPane
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.RowConstraints
import javafx.scene.control.cell.PropertyValueFactory

dynamic component PurchaseTab {
	AnchorPane{
		SplitPane {
			dividerPositions : 0.4
			,orientation : "VERTICAL"
			,prefHeight : 500.0
			,call AnchorPane#topAnchor : 0.0
			,call AnchorPane#bottomAnchor : 0.0
			,call AnchorPane#leftAnchor : 0.0
			,call AnchorPane#rightAnchor : 0.0
			,items : [
				AnchorPane {
					styleClass : "background_style"
					,prefHeight : 200.0
					,call SplitPane#resizableWithParent : false
					,HBox {
						alignment : "TOP_CENTER"
						,prefHeight : 26.0
						,prefWidth : 308.0
						,call AnchorPane#topAnchor : 5.0
						,call AnchorPane#leftAnchor : 0.0
						,call AnchorPane#rightAnchor : 0.0
						,children : [
							Button id newPurchase {
								onAction : controllermethod newPurchaseButtonClicked
								,text : "New Purchase"
							}
							,Button id submitPurchase {
								onAction : controllermethod submitPurchaseButtonClicked
								,text : "Confirm"
							}
							,Button id cancelPurchase {
								onAction : controllermethod cancelPurchaseButtonClicked
								,text : "Cancel" 
							}
						]
					}
					,TitledPane {
						alignment : "CENTER"
						,collapsible : false
						,animated : false
						,contentDisplay : "BOTTOM"
						,text : "Product"
						,call AnchorPane#topAnchor : 45.0
						,call AnchorPane#leftAnchor : 10.0
						,call AnchorPane#rightAnchor : 10.0
						,call AnchorPane#bottomAnchor : 5.0
						,content : GridPane {
							prefWidth : 214.0
							,styleClass: "sub_background"
							,columnConstraints : ColumnConstraints {
								hgrow : "SOMETIMES"
								,maxWidth : 10000.0
								,minWidth : 10.0
								,prefWidth : 50.0
							}
							,columnConstraints : ColumnConstraints {
								hgrow : "SOMETIMES"
								,maxWidth : 10000.0
								,minWidth : 10.0
								,prefWidth : 50.0
							}
							,columnConstraints : ColumnConstraints {
								hgrow : "SOMETIMES"
								,maxWidth : 10000.0
								,minWidth : 10.0
								,prefWidth : 50.0
							}
							,columnConstraints : ColumnConstraints {
								hgrow : "SOMETIMES"
								,maxWidth : 10000.0
								,minWidth : 10.0
								,prefWidth : 100.0
							}
							,rowConstraints : RowConstraints {
								minHeight : 30.0
								,prefHeight : 30.0
								,vgrow : "SOMETIMES"
							}
							,rowConstraints : RowConstraints {
								minHeight : 30.0
								,prefHeight : 30.0
								,vgrow : "SOMETIMES"
							}
							,rowConstraints : RowConstraints {
								minHeight : 30.0
								,prefHeight : 30.0
								,vgrow : "SOMETIMES"
							}
							,rowConstraints : RowConstraints {
								minHeight : 30.0
								,prefHeight : 30.0
								,vgrow : "SOMETIMES"
							}
							,children : [
								Label {
									text : "Bar Code:"
								}
								,Label {
									text : "Amount:"
									,call GridPane#rowIndex : 1
								}
								,Label {
									text : "Name:"
									,call GridPane#rowIndex : 2
								}
								,Label {
									text : "Price"
									,call GridPane#rowIndex : 3
								}
								,Button id addItemButton {
									mnemonicParsing : false
									,text : "Add to cart"
									,call GridPane#rowIndex : 1
									,call GridPane#columnIndex : 2
									,call GridPane#columnSpan : 2
									,call GridPane#rowSpan : 2
									,alignment :"CENTER"
									,call GridPane#Halignment : "CENTER"
									,call GridPane#Valignment : "CENTER"
									,onAction : controllermethod addItemEventHandler
								}
								,TextField id barCodeField {
									call GridPane#columnIndex : 1
								}
								,TextField id quantityField {
									call GridPane#columnIndex : 1
									,call GridPane#rowIndex : 1
								}
								,TextField id nameField {
									call GridPane#columnIndex : 1
									,call GridPane#rowIndex : 2
								}
								,TextField id priceField {
									call GridPane#columnIndex : 1
									,call GridPane#rowIndex : 3
								}
							]
						}
						
					}	
					
				}
				,AnchorPane {
					TitledPane {
						animated : false
						,alignment : "CENTER"
						,collapsible : false
						,text : "Shopping Cart"
						,call AnchorPane#topAnchor : 0.0
						,call AnchorPane#bottomAnchor : 0.0
						,call AnchorPane#leftAnchor : 0.0
						,call AnchorPane#rightAnchor : 0.0
						,content : TableView id purchaseTableView {
							styleClass : "blue_style"
							,columns : TableColumn {
								text : "Id"
								,cellValueFactory : PropertyValueFactory {
										property : "id"
								}
							}
							,columns : TableColumn {
								text : "Name"
								,cellValueFactory : PropertyValueFactory {
										property : "name"
								}
							}
							,columns : TableColumn {
								text : "Price"
								,cellValueFactory : PropertyValueFactory {
										property : "price"
								}
							}
							,columns : TableColumn {
								text : "Quantity"
								,cellValueFactory : PropertyValueFactory {
										property : "quantity"
								}
							}
							,columnResizePolicy : const TableView#CONSTRAINED_RESIZE_POLICY
						}
					}
				}
			]
		}
	}
}
